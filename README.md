
## APPRISE PARSE
----------------

Application support for parsing files, mostly Excel files.


A `ResourceParser` parses the contents of a `Blob` into an array of typed values.  

> besides the data, the `ResourceParseOutcome` includes the `ParseIssue`s encountered in the process. 

A `FileResource` complements a `Blob` with an identifier and a name. 

`useMultiParser()` feeds multiple `FileResource`s to a `ResourceParser`, integrating the results. It merges the data and indexes the issues by resource.

`useParseAggregator()` is related, but it feeds a single `FileResource`s to multiple `ResourceParser`s, integrating the results. 
It merges the data and the issues.

Our resource parsers support files in two formats: `xslx` and `json`. 

The first is our _public format_, ie. the main format of uploads presented to the parser.
The second is our format for record serialisation, but we can re-ingest it as an _internal format_.

The parsing process is logically split in a number of phases:

- read raw file contents, be them binary (`xlsx`) or text-based (`json`).
- parse file contents into the model defined by the file format, be a workbook (`xlsx`) or array of objects (`json`).
- convert the model defined by the format into object models.

As it turns out, much of the last phase no longer depends on the input format.
To avoid redundancy, we code it only for `json` and then split further the last phase for `xlsx` as follows:

- convert a workbook into an array of *row* objects.
- convert row objects into domain objects.

For this reason, `json` is our _canonical model_.

> if we were to support a third format, we would repeat this two-step mapping.

`useDefaultParser()` returns a `ResourceParser` that kicks off parsing for all submission types.

TIt inspects the mime type of the file, sets up parsing as a busy-waiting task, and delegates to the format-specific parsers returned by `useJsonParser()` or `useXlsxParser()`. 

Each *format parser* get a generic file parser from `useFileParser()` and uses it to extract the raw file contents as binary data or text. 
The `json` parser then parses the text into an array of one or more objects.
The `xlsx` parser feed the binary contents to a library and gets back a workbook object with an associated api.

Both parsers then delegate the rest of the parsing process to other parsers.
The `json` parser passes the array of object to a _model parser_.
The `xslx` parsers passes the workbook to a _workbokk parser_.

All model parsers iterate over objects and map them to `GenericRecord`s, preprocessing each field as appropriate for the target submission type.
`useDefaultModelParser()` returns a parser that takes care of object iteration and general error handling.
All model parsers customise it with a _row parser_.
`useDefaultRowParser()` repeates the pattern: parses a row for the aspects that are common to all row parsers and then delegates to type-specific parser.

Again, we use the same pattern to simplify the implementation of workbook parsers.
`useDefaultWorkbookParser()` implements a generic, config-driven strategy to find and parse data in workbook sheets.
All workbook parsers customise it by passing to it a model parser and an indication of the target record type (which is used to consult the relevant config.)


The essence of the sheet parsing strategy is:

- find the _cornerstone_ for the data in the sheet, i.e. top-left most cell that begins the header row of a table with the data to extract (row and col offsets are possible). 
- parse all the rows below the header into json objects that jave header cell contents as the key che row cell content as the value (we call it _raw parse_). The `xlsx` library does this.
- convert the raw parse objects into objects in the canonical model looking up the config for a correspondence between header cell contents and keys in the canonical model.

Based on configuration, some sheets can be explicitly skipped, and others can be explicitly included.
Based on this, we collect or don't collect warnings about missing data.