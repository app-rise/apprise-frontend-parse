
import { useT } from 'apprise-frontend-core/intl/language'
import { defaultParseOptions, ParseContext, ParseIssue, ResourceParseOutcome, ResourceParser } from './model'





export const useParseAggregator = <T, S>(parsers: ResourceParser<T,S>[]) => {

    const t = useT()

    return async (files: File[], ctx = defaultParseOptions as ParseContext<S>) => {

        // 1. parses all bytestreamss bound to resources concurrently. 
        //    indexes all future outcomes by their stream id.
        //    converts failures or empty outcomes into parsing issues.

        const parseTasks = files.flatMap(file => parsers.map(parser =>

            parser?.({ id: file.name, name: file.name, file }, ctx).then(outcome => {

                // adds an additional issue for "empty" files.
                if (outcome.data.length === 0 && ctx.emptyFileIssue)
                    outcome.issues.push({ message: t('parse.empty_resource', { file: file.name }), type:  ctx.emptyFileIssue })

                return outcome
            })
                // exploded: adds a single issue derived from error.
                .catch(error => {

                    const issues = [{ message: error.message ?? t('parse.unspecified_error'), type: 'error' } as ParseIssue]

                    return { data: [], issues } as ResourceParseOutcome<T>  // an error produces no data: streamlines post-processing.

                })

        )
            .filter(o => !!o))

        //  2. wait for all parsing outcomes to accumulate all their records and issues.

        return Promise.all(parseTasks).then(outcomes => outcomes.reduce(

            ({ data, issues }, outcome) => ({

                // accumulate errors, if any.
                data: [...data, ...outcome.data ?? []],

                // accumulate issues, if any.
                issues: outcome.issues.length > 0 ? [...issues, ...outcome.issues] : issues

            })
            ,

            { data: [], issues: [] } as ResourceParseOutcome<T>))

    }
}